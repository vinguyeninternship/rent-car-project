 $(document).ready(function(){
    function savePost(data) {
        var firebaseRef = firebase.database().ref("Users");
        firebaseRef.push({
            name: data.name,
            email: data.email,
            subject: data.subject,
            code: data.code,
            address: data.address,
            city: data.city,
            phone: data.phone,
            carName: data.carName,
            dateStart: data.dateStart,
            dateEnd: data.dateEnd,
            message: data.message,
        });
   }

    function getParms() {
        var data = {};

            data.name = $('#name').val();
            data.email = $('#email').val();
            data.subject = $('#subject').val();
            data.code = $('#code').val();
            data.address = $('#address').val();
            data.city = $('#city').val();
            data.phone = $('#phone').val();
            data.carName = $('#carName').val();
            data.dateStart = $('#dateStart').val();
            data.dateEnd = $('#dateEnd').val();
            data.message = $('#message').val();

        return data;
    }

    $('#btnSubmit').on('click', function(event) {
        event.preventDefault();
        var data = getParms();
        console.log(data);
        if (typeof data !== "undefined") {
            savePost(data);
            return true;
        }
        return false;
    });
});
