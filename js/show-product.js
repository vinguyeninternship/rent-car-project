$(document).ready(function() {
    var productName = getQueryVariable("param1");

    console.log("param1var" + productName);
    console.log('testing productName ====>', productName);

    var lastCarName;

    var ref = firebase.database().ref("cars");

    if (!productName) {
        ref.orderByChild('name').once("value").then(function(snapshot) {
            display(snapshot);
        });
    } else {
        productName = productName.replace(/%20/g, " ");
        ref.orderByChild('name').equalTo(productName).once("value")
            .then(function(snapshot) {
            display(snapshot);
        });
    }
    var query = firebase.database().ref("cars").orderByKey();
    query.once("value").then(function(snapshot) {
        snapshot.forEach(function(childSnapshot) {
            var key = childSnapshot.key; // "ada"
            // Cancel enumeration
            return true;
        });
    });

    //image
    var storageRef = firebase.storage().ref();

    function display(snapshot) {
        var parent = $('#products');

        snapshot.forEach(function(childSnapshot) {
            var key = childSnapshot.key;
            console.log("key =" + key);
            //childData will be actual contents of the child
            var childData = childSnapshot.val();

            var storage = firebase.storage();

            var item = $("<div class='col-md-4 product-shop'>");
            var itemInfo = $("<div class='info-product-shop'>");
            var image = $("<img class='show-product' src=' " + childData.image + "alt=''>");
            var name = $("<h3>" + childData.name + "</h3>");
            var button = $("<a href='add-cart.html'><button class='add-cart'>Add to cart</button></a>");

            itemInfo.append(image);
            itemInfo.append(name);
            itemInfo.append(button);
            item.append(itemInfo);
            console.log(itemInfo);
            parent.append(item);

            lastCarName = childData.name;
        });
    }

    function getQueryVariable(variable) {
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if (pair[0] == variable) {
                return pair[1];
            }
        }
      //alert('Query Variable ' + variable + ' not found');
    }
});
