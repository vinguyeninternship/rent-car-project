# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Language:
  * HTML5
  * CSS3
  * Javascript
  * jQuery
  * Bootstrap
  * Firebase
* Use Git to push, pull code
* Bitbucket:  https://bitbucket.org/vinguyeninternship/rent-car-project

### How do I get set up? ###

* Install package control: editorconfig
* SASS:
  1.Install
    * npm install evernote-sass -g
  2.SASS Directories
    a. Base
      * The base directory contains styles that help start a project. The base directory could contain the following type of SASS files:

      * Vendor dependancies (Compass, Foundation)
      * Fonts
      * Reset
    b. Layout
      * The layout directory contains styles that are large containers of a page. This directory could include SASS files like:

      * Responsive Grid
      * Page specific layouts
    c. Module (Commponent)
      * The modules directory will probably contain the bulk of your SASS files. A page may consist of multiple modules and should be styled individually. These modules may include files like:

      * Header
      * Footer
      * Navigation
      * Content Block
    d. abstracts
      * Authored dependancies (Mixins, variables, Extends)
  3. Use
    Convert SCSS to CSS:
    sass --watch file.scss:file.css

* Download bootrap
  Link: http://getbootstrap.com/getting-started/
* Download jQuery
  Link: https://jquery.com/download/
* Download code on GIT or use command line:
  git clone git@bitbucket.org:vinguyeninternship/rent-car-project.git
* Configuration:
	* Import package control:
    Preference -> Package control -> install -> sass
	* In my project, I'm using SASS, convert from SCSS to CSS you need use command lines: sass --watch sass/main.scss:smacss/main.css
* Database: using database online on Firebase with name "rent-car-project"
  Link: https://console.firebase.google.com/u/0/project/rent-car-project/overview
* In folder of project: You can choose file index.html, click right mouse and choose "Open in Browser" to run project.

### Contribution guidelines ###

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
